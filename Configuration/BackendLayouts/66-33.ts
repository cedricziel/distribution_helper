backend_layout {
	colCount = 3
	rowCount = 1
	rows {
		1 {
			columns {
				1 {
					name = Main
					colspan = 2
					colPos = 0
				}
				2 {
					name = Aside
					colPos = 1
				}
			}
		}
	}
}