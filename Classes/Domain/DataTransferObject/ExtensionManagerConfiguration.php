<?php
namespace T3easy\DistributionHelper\Domain\DataTransferObject;

/**
 * Created by PhpStorm.
 * User: jan
 * Date: 06.01.14
 * Time: 18:04
 */


use TYPO3\CMS\Core\Utility\GeneralUtility;

class ExtensionManagerConfiguration {

	/**
	 * @var array
	 */
	protected $packageCategories = array();

	/**
	 * @var array
	 */
	protected $packageKeys = array();

	/**
	 * @var array
	 */
	protected $packageKeysToExclude = array();

	/**
	 * @param array $configuration
	 */
	public function __construct(array $configuration) {
		foreach ($configuration as $key => $value) {
			if (property_exists(__CLASS__, $key)) {
				$setter = 'set' . ucfirst($key);
				$this->$setter($value);
			}
		}
	}

	/**
	 * @param string $packageCategories
	 */
	public function setPackageCategories($packageCategories) {
		$this->packageCategories = GeneralUtility::trimExplode(',', $packageCategories);
	}

	/**
	 * @return array
	 */
	public function getPackageCategories() {
		return $this->packageCategories;
	}

	/**
	 * @param string $packageKeys
	 */
	public function setPackageKeys($packageKeys) {
		$this->packageKeys = GeneralUtility::trimExplode(',', $packageKeys);
	}

	/**
	 * @return array
	 */
	public function getPackageKeys() {
		return $this->packageKeys;
	}

	/**
	 * @param string $packageKeysToExclude
	 */
	public function setPackageKeysToExclude($packageKeysToExclude) {
		$this->packageKeysToExclude = GeneralUtility::trimExplode(',', $packageKeysToExclude);
	}

	/**
	 * @return array
	 */
	public function getPackageKeysToExclude() {
		return $this->packageKeysToExclude;
	}

}