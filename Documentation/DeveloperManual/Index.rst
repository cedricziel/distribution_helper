﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../Includes.txt


.. _developer-manual:

Developer manual
================

To load your backend layout files automatically from your extension:

#. Put your backend layout files in */Configuration/BackendLayouts/*
#. Set the category to *distribution*
#. Place the icons for the layouts in */Resources/Public/Icons/BackendLayouts/* the must have the same filename as
   the backend layout file.
#. The filename is used as description and part of the identifier. You can set a more user friendly description for
   the backend layout in */Resources/Private/Language/locallang.xlf* (or locallang.xml).
#. The layouts now appear in the page configuration tab appearance

.. figure:: ../Images/PageAppearance.png
	:width: 850
	:alt: Page appearance tab

	The page appearance tab
