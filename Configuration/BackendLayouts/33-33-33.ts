backend_layout {
	colCount = 3
	rowCount = 1
	rows {
		1 {
			columns {
				1 {
					name = Left
					colPos = 0
				}
				2 {
					name = Center
					colPos = 1
				}
				3 {
					name = Right
					colPos = 2
				}
			}
		}
	}
}
